//This class is the controller class used to map different request comming from the api hits

package com.example.testingrest.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.List;

@RestController
public class StudentController {

    // The dependency injection happens here and it calls the Repository named as  StudentRepository
    @Autowired
    StudentRepository studentRepository;

    // get mapping it map the get Request and returns a List of students in the database
    @GetMapping ("/getAllStudents")
    public List<Student> getStudents(){
        return studentRepository.findAll(); //findAll() returns the all the instance of the Student class
    }

    //get mapping this function is required to hit the api based on the roll no of the student this
    // get mapping is used to get the student details by inputting its roll no
    @GetMapping("/getStudent/{roll}")
    public Student getStudentByRoll(@PathVariable int roll ){
        return studentRepository.findByRoll(roll); // findByRoll () function is used to get the Students Details by its roll number provided and its a method inside the StudentRepository Interface

    }

    //This method is used to function the postMapping it tests the wheather the following method is available
    //If yes the if something is changed it will update else it will add the data to the database
    // It only accepts JSON type data

    @PutMapping (path="/addStudents/")
    public ResponseEntity<Void> addStudent( @RequestBody Student requestedStudent){
        Student newStudent = new Student();  //Instantiating a Student Object

        //If the request is null then it will Retuen a noContent object
        
        if(requestedStudent==null)
            return ResponseEntity.noContent().build();

        // If the Student already exists then update his values of the student and put it into the database
        else if( getStudentByRoll(requestedStudent.getRoll())!=null && !getStudentByRoll(requestedStudent.getRoll()).getName().equals(requestedStudent.getName()) ){
            newStudent.setName(requestedStudent.getName()); //set the name of the student
            int roll = requestedStudent.getRoll(); //Getting the roll no of the student
            newStudent.setRoll(roll); //set the role of the student
            studentRepository.save(newStudent); //saving the new Student object into the student database
            URI newStudentData = ServletUriComponentsBuilder.fromCurrentRequest().path("/"+roll).buildAndExpand(roll).toUri(); //Creating the return uri and returning it

            return ResponseEntity.created(newStudentData).build();
        }

        //If the student data already not in the database then save the new student and update the database
        else{
            newStudent.setRoll(requestedStudent.getRoll());
            newStudent.setName(requestedStudent.getName());
            URI newStudentData = ServletUriComponentsBuilder.fromCurrentRequest().path("/"+requestedStudent.getRoll()).buildAndExpand(requestedStudent.getRoll()).toUri(); //Creating the uri to check in the testing method whether its available in that part or not

            studentRepository.save(newStudent); //Saving the data into the database

            return ResponseEntity.created(newStudentData).build(); //Returning the data entity
        }
    }
    // Delete mapping to delete/remove any data from the databse and returning it

    @Transactional
    @DeleteMapping("/deleteById/{roll}")
    public Student deleteUserById(@PathVariable int roll){
        Student ret = getStudentByRoll(roll);// saving the data for the future returning
        studentRepository.deleteByRoll(roll); //Deleting the data from the database
        return ret; //Returning the Deleted Student Data
    }

}
