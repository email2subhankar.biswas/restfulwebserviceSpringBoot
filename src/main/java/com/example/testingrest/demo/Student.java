// This is a DAO class for using it as a Model of each student having two entities the first one is roll which describes
//the unique rollno of each student and the second one is the name which basically is the name of that particular
//student.



package com.example.testingrest.demo;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data  //Lombok annotations for the getters setters and toStrig Methods
@AllArgsConstructor //Lombok methode for creating a constructor with all the argument as parameter
@Entity //Jpa annotation describes the ORM relation between the class and  the mysql database
public class Student {

    @Id // Describes that the variable is used as the primary key in the entity model
    //@GeneratedValue(strategy= GenerationType.IDENTITY) //It autoGenerate the primary Key
    private int roll; //describes the roll of the student which can be uniquely find a particular student

    private String name; //describes the name of the student which is a not null quantity

    // Constructor for the class
    public Student() {

    }
}
