// The interface is a Dao Repository which extends the JPA Repository and the jpa Repository takes two Entity
// one is the Dao Class Name which is Studrent and another is the Primary KEy data type which is integer


package com.example.testingrest.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Integer>{
    public Student findByRoll(int roll); // This method findByRoll() is given here its used to the details of the stusent by providing the roll of that student


    public Student deleteByRoll(int roll);
}
