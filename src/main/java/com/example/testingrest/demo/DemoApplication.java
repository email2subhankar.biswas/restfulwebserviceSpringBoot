//This is the starting point of the project

package com.example.testingrest.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    //The main method

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
