//This is the class implements the Integration Testing of the class StudentController it checks wheather all the components are Integrated successfully or not



package com.example.testingrest.demo;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;


@RunWith(SpringRunner.class) //To run the application that SpringRunner extends SpringJUnit4ClassRunner
@SpringBootTest(classes=DemoApplication.class, webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT) //Initializes the Spring boot Integrtion Test on a random port

 public class DemoApplicationITTests {

    @LocalServerPort //Autowire  the Port to a int so that can be used in URL
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();


    //This method checks the integrity of the getAllStudents() funtion
    //Checks wheather all the data can be fired from the response string

    @Test
    public void getAllStudentTest() throws Exception{
        HttpEntity<String> entity =  new HttpEntity<String>(null,headers);
        ResponseEntity<String> response= restTemplate.exchange(createURLWithPort("/getAllStudents"),HttpMethod.GET,entity,String.class);
        String actualBody = "{roll:2,name:subho},{roll:3,name:subhankar}";
        //System.out.println(actualBody.toString());
        JSONAssert.assertEquals(actualBody, response.getBody().toString(),false);
    }

    //Method to test getStudentById() function and its an Integration Test and it prints the Query if successful on the console
    @Test
    public void getStudentsById() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null,headers); //For adding the request Header in feature
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/getStudent/2"), HttpMethod.GET,entity,String.class); //Fire a get request to the URI and get the response as a String
        String expected = "{roll:2,name:subho}"; //Expected String that should match the response
        JSONAssert.assertEquals(expected,response.getBody().toString(),false); //Asserts that the response the expected fields
    }

    //Method to test the addStudent() put and post methode it checks wheather a data can be successfully
    // Updated if it is already present in the database and if not the if it can be successfully inserted into the database or not
    // It should return a ResponseEntity object an url describing the url of the inserted data

    @Test
    public void addStudentTest() throws Exception {
        Student testStudent = new Student(3,"subhankar"); //Creating an dummy Student to test the addStudent() methode if it can perform PUT and POST request successfully

        HttpEntity<Student> entity = new HttpEntity<Student>(testStudent,headers); //For adding request Headers in future
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/addStudents/"),HttpMethod.PUT,entity,String.class); //Fire an POST or PUT request based on the type of request and return the response as the URI of the path to the request headers
        String actualHeaders= response.getHeaders().get(HttpHeaders.LOCATION).get(0); // get the request from the Header Locations
        Assert.assertTrue(actualHeaders.contains("/addStudents/")); //Asserts if the URL returned have the following part or not

    }

    // This methode checks wheather we can delete an object from the database successfully
    // If yes then it will delete the data from the database and return the deleted object

    @Test
    public void deleteUserTest() throws Exception{
        HttpEntity<String> entity = new HttpEntity<String>(null,headers); //Adding Request Headers in future
        ResponseEntity<String> response=restTemplate.exchange(createURLWithPort("/deleteById/1"),HttpMethod.DELETE,entity,String.class); //Fire the delete request and deletes the data from the databse
        String expected="{roll:1,name:subs}"; //Expected return object that has already been
        JSONAssert.assertEquals(expected,response.getBody().toString(),false); //Asserts wheather the returned and actual data are same
    }

    // This is a helper method that returns the complete URL by inputting the URI
    public  String createURLWithPort(String uri){
        return "http://localhost:"+port+uri;
    }

}
