//This class checks the each unit and Its an unit testing class

package com.example.testingrest.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.binding.When;
import org.apache.catalina.mapper.Mapper;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.platform.engine.TestExecutionResult;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DemoApplicationUTTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    StudentRepository studentRepository;

    @Test
    public void getAllStudent() throws Exception{
        when(studentRepository.findAll()).thenReturn(Collections.emptyList());

        MvcResult mvcResult= mockMvc.perform(MockMvcRequestBuilders.get("/getAllStudents").accept(MediaType.APPLICATION_JSON)).andReturn();

        System.out.println(mvcResult.getResponse().getStatus());

        verify(studentRepository).findAll();
    }

    @Test
    public void getStudentByRollTest() throws Exception{
        when(studentRepository.findByRoll(1)).thenReturn(new Student());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/getStudent/1").accept(MediaType.APPLICATION_JSON)).andReturn();
        System.out.println(mvcResult.getResponse());
        verify(studentRepository).findByRoll(1);
    }

    @Test
    public void addStudent() throws Exception {
        Student dummyStudent=new Student(4,"subs");
       Mockito.when(studentRepository.save(Mockito.any(Student.class))).thenReturn(dummyStudent);

       MvcResult mvcResult=mockMvc.perform(MockMvcRequestBuilders.put("/addStudents/").
               content(asJsonString(dummyStudent)).
               contentType(MediaType.APPLICATION_JSON).
               accept(MediaType.APPLICATION_JSON)).
               andExpect(MockMvcResultMatchers.status().is(201)).andReturn();



        Assertions.assertThat(mvcResult).isNotNull();

       String userJson = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
       System.out.println(userJson);
       Assertions.assertThat(userJson).isNotEmpty();



    }

    @Test
    public void deleteStudentById() throws Exception{
        Mockito.when(studentRepository.deleteByRoll(1)).thenReturn(new Student());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/deleteById/1").accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        Assertions.assertThat(mvcResult).isNotNull();
       verify(studentRepository).deleteByRoll(1);

    }

    public static String asJsonString(final Object obj){
        try{
           return  new ObjectMapper().writeValueAsString(obj);
        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }


}
